############################################# Makefile para o jogo ############################################

################################################### Comandos #################################################

all: jogo.exe

jogo.exe: glm.o personagem.o pista.o borracha.o corretivo.o utilidades.o skybox.o obstaculos.o main.o
	-@ gcc -g -o jogo.exe glm.o utilidades.o personagem.o pista.o skybox.o corretivo.o borracha.o obstaculos.o main.o -lglut -lGL -lGLU -lSOIL -lm

glm.o: glm.c glm.h
	-@ gcc -o glm.o -c glm.c -Wall

utilidades.o: glm.h utilidades.c utilidades.h
	-@ gcc -o utilidades.o -c utilidades.c -Wall

personagem.o: personagem.c personagem.h
	-@ gcc -o personagem.o -c personagem.c -Wall

borracha.o: utilidades.h glm.h objetosEmCodigo/borracha.c objetosEmCodigo/borracha.h
	-@ gcc -o borracha.o -c objetosEmCodigo/borracha.c -Wall

corretivo.o: objetosEmCodigo/corretivo.c glm.h utilidades.h objetosEmCodigo/corretivo.h
	-@ gcc -o corretivo.o -c objetosEmCodigo/corretivo.c -Wall

pista.o: glm.h objetosEmCodigo/pista.c objetosEmCodigo/pista.h
	-@ gcc -o pista.o -c objetosEmCodigo/pista.c -Wall

skybox.o: glm.h utilidades.h objetosEmCodigo/skybox.h objetosEmCodigo/skybox.c
	-@ gcc -o skybox.o -c objetosEmCodigo/skybox.c -Wall

obstaculos.o: obstaculos.h glm.h utilidades.h objetosEmCodigo/corretivo.h objetosEmCodigo/borracha.h
	-@ gcc -o obstaculos.o -c obstaculos.c -Wall

main.o: main.c
	-@ gcc -o main.o -c main.c -Wall

clean:
	-@ rm -rf *.o

rmproper:
	-@ rm -rf jogo.exe

run: jogo.exe
	-@ ./jogo.exe
