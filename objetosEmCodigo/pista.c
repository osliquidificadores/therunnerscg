#include <GL/glew.h>
#include <GL/freeglut.h>

#include "../glm.h"
#include "../utilidades.h"
#include "pista.h"

// Procedimentos
void inicializaPistas() {
  // Inicializando pista 0
  pistas[0].planoList = glGenLists(1);

  glNewList(pistas[0].planoList, GL_COMPILE);
    glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, texturas[CHAO]);
      // Tentando repetir textura
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
      glBegin(GL_TRIANGLE_STRIP);
        glTexCoord2f(0, 0); glVertex3f(-5, -2.95, 2);
        glTexCoord2f(0, 5); glVertex3f(-5, -2.95, -15.5);
        glTexCoord2f(5, 0); glVertex3f(5, -2.95, 2);
        glTexCoord2f(5, 5); glVertex3f(5, -2.95, -15.5);
      glEnd();
    glDisable(GL_TEXTURE_2D);
  glEndList();

  pistas[0].posicaoZ = 0.0;

  // Inicializando pista 1
  pistas[1].planoList = glGenLists(1);

  glNewList(pistas[1].planoList, GL_COMPILE);
    glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, texturas[CHAO]);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
      glBegin(GL_TRIANGLE_STRIP);
        glTexCoord2f(0, 0); glVertex3f(-5, -2.95, 2);
        glTexCoord2f(0, 5); glVertex3f(-5, -2.95, -15.5);
        glTexCoord2f(5, 0); glVertex3f(5, -2.95, 2);
        glTexCoord2f(5, 5); glVertex3f(5, -2.95, -15.5);
      glEnd();
    glDisable(GL_TEXTURE_2D);
  glEndList();

  pistas[1].posicaoZ = -17.5;
}

void transladaPistas() {
  if(pistas[0].posicaoZ >= 17.0){
    pistas[0].posicaoZ = -17.0;
    return ;
  }

  if(pistas[1].posicaoZ >= 17.0){
    pistas[1].posicaoZ = -17.0;
    return ;
  }

  pistas[0].posicaoZ += 0.07;
  pistas[1].posicaoZ += 0.07;
}

void desenhaPistas() {
  // Desenhando pista 0
  glPushMatrix();
    glTranslatef(0, 0, pistas[0].posicaoZ);
    glCallList(pistas[0].planoList);
  glPopMatrix();

  // Desenhando pista 1
  glPushMatrix();
    glTranslatef(0, 0, pistas[1].posicaoZ);
    glCallList(pistas[1].planoList);
  glPopMatrix();
}
