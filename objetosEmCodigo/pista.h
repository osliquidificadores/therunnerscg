
// Estrutura definindo um plano
typedef struct plano {
  // displayList para desenhar o plano da pista
  GLuint planoList;
  // Definem a posição do plano
  GLfloat posicaoZ;
} PLANO;

PLANO pistas[2];

// Procedimentos

// Inicializa as pistas
void inicializaPistas();
// Translada as pistas ao longo do eixo Z
void transladaPistas();
// Desenha as pistas em suas posições
void desenhaPistas();
