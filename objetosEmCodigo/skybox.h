
// Criação de GLMmodel Skybox
GLMmodel *skyboxBase;

// Criação de displayList para o skybox
GLuint skyboxList;

// Procedimentos
// Cria o skybox a partir do arquivo .obj
void inicializaSkybox();

// Desenha o skybox
void desenhaSkybox();
