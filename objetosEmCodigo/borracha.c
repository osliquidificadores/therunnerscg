#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>

#include "../glm.h"
#include "../utilidades.h"
#include "borracha.h"

// Procedimentos
void inicializaBorracha(){
  // Cria um GLMmodel para o corretivo
  borrachaBase = preparaModelo("arquivosBlender/arquivosOBJ/borracha.obj");
  // Cria um displayList para o corretivo
  borrachaList = glmList(borrachaBase, GLM_SMOOTH | GLM_TEXTURE | GLM_MATERIAL);
}

void desenhaBorracha(GLfloat x, GLfloat y, GLfloat z, GLfloat angulo){
  glPushMatrix();
    glScalef(0.25, 0.25, 0.25);
    // Translada a borracha
    glTranslatef(x, y + 0.8, z);
    // Rotaciona a borracha
    glRotatef(angulo, 0, 1, 0);
    // Chama a displayList da borracha
    glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, texturas[BORRACHA]);
      glCallList(borrachaList);
    glDisable(GL_TEXTURE_2D);
  glPopMatrix();
}
