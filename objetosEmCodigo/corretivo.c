#include <GL/glew.h>
#include <GL/freeglut.h>
#include <math.h>

#include "../glm.h"
#include "../utilidades.h"
#include "corretivo.h"

// Procedimentos
void inicializaCorretivo(){
  // Cria um GLMmodel para o corretivo
  corretivoBase = preparaModelo("arquivosBlender/arquivosOBJ/corretivo.obj");
  // Cria um displayList para o corretivo
  corretivoList = glmList(corretivoBase, GLM_FLAT | GLM_TEXTURE | GLM_MATERIAL);
}

void desenhaCorretivo(GLfloat x, GLfloat y, GLfloat z, GLfloat angulo){
  glPushMatrix();
    glScalef(0.25, 0.25, 0.25);
    // Translada o corretivo
    glTranslatef(x, y + 1.3, z);
    // Rotaciona o corretivo
    glRotatef(90, 0, 0, 1);
    glRotatef(angulo, 0, 1, 0);
    // Chama a displayList do corretivo
    glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, texturas[CORRETIVO]);
      glCallList(corretivoList);
    glDisable(GL_TEXTURE_2D);
  glPopMatrix();
}
