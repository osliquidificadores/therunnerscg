#include <GL/glew.h>
#include <GL/freeglut.h>

#include "../glm.h"
#include "../utilidades.h"
#include "skybox.h"

// Procedimentos
void inicializaSkybox(){
  // Cria um GLMmodel para a skybox
  skyboxBase = preparaModelo("arquivosBlender/arquivosOBJ/skybox.obj");
  // Cria um displayList para a skybox
  skyboxList = glmList(skyboxBase, GLM_FLAT | GLM_TEXTURE | GLM_MATERIAL);
}

void desenhaSkybox(){
  glPushMatrix();
    glScalef(5, 5, 9);
    // Chama a displayList da skybox
    glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, texturas[SKYBOX]);
      glCallList(skyboxList);
    glDisable(GL_TEXTURE_2D);
  glPopMatrix();
}
