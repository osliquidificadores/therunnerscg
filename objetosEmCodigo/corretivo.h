
// Criação de GLMmodel Corretivo
GLMmodel *corretivoBase;

// Criação de displayList para o corretivo
GLuint corretivoList;

// Procedimentos
// Lê o corretivo.obj e cria uma displayList para o corretivo
void inicializaCorretivo();

// Desenha o corretivo
void desenhaCorretivo(GLfloat x, GLfloat y, GLfloat z, GLfloat angulo);
