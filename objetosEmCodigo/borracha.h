
// Criação de GLMmodel Borracha
GLMmodel *borrachaBase;

// Criação de displayList para a borracha
GLuint borrachaList;

// Procedimentos
// Lê o corretivo.obj e cria uma displayList para o corretivo
void inicializaBorracha();

// Desenha o corretivo
void desenhaBorracha(GLfloat x, GLfloat y, GLfloat z, GLfloat angulo);
