#include <SOIL/SOIL.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <string.h>
#include <math.h>

#include "glm.h"
#include "utilidades.h"
#include "objetosEmCodigo/corretivo.h"
#include "objetosEmCodigo/borracha.h"
#include "objetosEmCodigo/pista.h"
#include "objetosEmCodigo/skybox.h"
#include "obstaculos.h"
#include "personagem.h"

typedef enum tecla{
  UP = 0,
  DOWN,
  LEFT,
  RIGHT,
  SPACE,
  W,
  A,
  D,
  C,
  P,
  R,
  M
} TECLA;

int tecladoState[12] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
// GLuint texturas[2] = {0, 0};

GLfloat peDoBoneco = -12.0;
int auxSobe = 1;
double obstZ = -5;
double xMouse, yMouse;
//float cor[3] = { .5f, .5f, .5f }; // FOG
float cor[3] = {0, 0.75,1}; // FOG

int telaNUM[4] = {1, 0, 0, 0} ;

char String[10]; // String buffer


void inicializa(){

  // Inicializa as variáveis
  pausar = 0;
  vidas = 4;
  posiX = 0.0;
  posiY = YMIN;
  posiZ = 0.0;

  pontuacao = 0;

  glClearColor(cor[0], cor[1], cor[2], 1.0f);
  //glClearColor (0.231, 0.112, 0.016, 0.0);
  //glClearColor (1, 1, 1, 0.0);

  // Fogo
  glFogi(GL_FOG_MODE, GL_LINEAR);     // Linear, exp. ou exp²
  glFogfv(GL_FOG_COLOR, cor);         // Cor
  glHint(GL_FOG_HINT, GL_DONT_CARE);  // Não aplicar se não puder
  glFogf(GL_FOG_START, 15.0f);         // Profundidade inicial
  glFogf(GL_FOG_END, 20.0f);           // Profundidade final
  glEnable(GL_FOG);

  glClearColor (cor[0], cor[1], cor[2], 0.0);
  glShadeModel (GL_SMOOTH);
  glEnable(GL_DEPTH_TEST);
  inicializaCorretivo();
  inicializaBorracha();
  initTexturas();
  inicializaSkybox();
  iniciaListaObstaculos();
  inicializaPistas();
  glutTimerFunc(3000, spawnaObstaculo, 0);

  glColor3f (1.0, 1.0, 1.0);
}

void desenhaCenaInicial(){

  ConfiguraProjecao('o');
  // Habilita o uso de texturas		    // Habilita o uso de texturas
  desenhaTextura(100, 100, INICIAL);
  desenhaTextura(15, 10, BOTAO);
  glColor3f (cor[0], cor[1], cor[2]);
  escreveTexto(GLUT_BITMAP_HELVETICA_18, "COMECAR", -7, -3, 0);

  glTranslatef(0, -30, 0);
  desenhaTextura(15, 10, BOTAO);
  escreveTexto(GLUT_BITMAP_HELVETICA_18, "INSTRUCOES", -8, -3, 0);

  glTranslatef(0, -30, 0);
  desenhaTextura(15, 10, BOTAO);
  escreveTexto(GLUT_BITMAP_HELVETICA_18, "SAIR", -3, -3, 0);

  glutSwapBuffers();
}

void desenhaCenaComoJogar(){
  ConfiguraProjecao('o');
  desenhaTextura(100, 100, COMOJOGAR);

  glutSwapBuffers();
}

// Converte um número float em string
void floatParaString(char * destStr, int precision, float val)
{
    sprintf(destStr,"%f",val);
    destStr[precision] = '\0';
}

void desenhaCenaSair(){
  ConfiguraProjecao('o');
  desenhaTextura(100, 100, SAIR);

  floatParaString(String, 4, pontuacao);
  escreveTexto(GLUT_BITMAP_HELVETICA_18, "PONTUACAO: ", -23, -3, 0);
  escreveTexto(GLUT_BITMAP_HELVETICA_18, String, -3, -3, 0);

  glutSwapBuffers();
}

void desenha(){

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Tela inicial do jogo
  if (telaNUM[TELAINICIAL]) desenhaCenaInicial();
  else if (telaNUM[TELACOMOJOGAR]) desenhaCenaComoJogar();
  else if (telaNUM[TELASAIR]) desenhaCenaSair();
  else if (telaNUM[TELAJOGO] && !pausar){
    ConfiguraProjecao('p');

    // M O V I M E N T A  P A R A  E S Q U E R D A
    if (tecladoState[LEFT]){
      if (posiX > -1 * MAX_MAP)
        posiX -= 0.1;
    }
    // M O V I M E N T A  P A R A  D I R E I T A
    if (tecladoState[RIGHT]){
      if (posiX < MAX_MAP)
        posiX += 0.1;
    }
    // M O V I M E N T A  P A R A  F R E N T E
    if (tecladoState[UP]){
      if (posiZ < MAX_MAP)
        posiZ -= 0.1;
    }
    // M O V I M E N T A  P A R A  T R A S
    if (tecladoState[DOWN]){
      if (posiZ < MAX_MAP)
        posiZ += 0.1;
    }

    // P U L A R
    if (tecladoState[SPACE]){  // sobe
      if (posiY > YMAX && auxSobe == 1){
        posiY -= 0.075;
        peDoBoneco -= 0.075;
      }
      else if (posiY < YMIN){  // desce
        auxSobe = 0;
        posiY += 0.075;
        peDoBoneco += 0.075;
      }
      else if (posiY >= YMIN){ // desceu
        auxSobe = 1;
        tecladoState[SPACE] = 0;
      }
    }

    // POSIÇÃO DA CÂMERA
    // Posiciona a câmera pelo mause
    if (tecladoState[M]){
      gluLookAt(1*(xMouse-larguraJanela/2)/(larguraJanela/16), -1*(yMouse-alturaJanela/2)/(alturaJanela/16) + 3, 5.0, // Posisão da câmera
                0.0, 0.0, 0.0,  // Para onde vai olhar
                0.0, 1.0, 0.0   // Normal que diz onde é cima
              );
              // Câmera segue o personagem
    }
    else{
      gluLookAt(posiX/10, posiY, 5.0, // Posisão da câmera
                0.0, 0.0, 0.0,  // Para onde vai olhar
                0.0, 1.0, 0.0   // Normal que diz onde é cima
              );
    }


    // OBJETO DE teste
    /*if (obstZ > 7) obstZ = -20;
    obstZ += 0.2;
    glPushMatrix();
    glTranslatef (0.0, -1, obstZ);
    glColor3f(1,0,0);
      glBegin(GL_TRIANGLE_STRIP);
        glVertex3f(-1,-1,0);
        glVertex3f(-1,1,0);
        glVertex3f(1,-1,0);
        glVertex3f(1,1,0);
      glEnd();
      glColor3f(1.0, 1.0, 1.0);
    glPopMatrix();*/
    desenhaSkybox();

    desenhaPistas();

     //desenhaCorretivo(4.0, -2.0, -2.0, 180);
    //
    // desenhaCorretivo(6.0, -12, -6.0, 0);
    // //
    // desenhaBorracha(-6.0, -12.0, -6.0, 90);


    desenhaObstaculos();

    // DESENHA PERSONAGEM
    glColor3f (0, 0, 0);
    desenhaPersonagem(posiX, posiY, posiZ, vidas);
    glColor3f (1, 1, 1);

    glutSwapBuffers();
  }
}

// Calback para quando um teclado especial é solto
void tecladoSpecialUP(int key, int x, int y){
  switch (key) {

    case GLUT_KEY_LEFT:  // Seta para esquerda
      tecladoState[LEFT] = 0;
      //printf("x: %d ||  y: %d\n", x, y);
      break;

    case GLUT_KEY_RIGHT:  // Seta para direita
      tecladoState[RIGHT] = 0;
      break;

    case GLUT_KEY_UP:
      tecladoState[UP] = 0;
      break;

    case GLUT_KEY_DOWN:
      tecladoState[DOWN] = 0;
      break;

    default:
      break;
  }
}

// Callback para quando um teclado especial é pressionado
void tecladoSpecialDOWN(int key, int x, int y){
  switch (key) {
    case GLUT_KEY_LEFT:  // Seta para esquerda
      tecladoState[LEFT] = 1;
      break;

    case GLUT_KEY_RIGHT:  // Seta para direita
      tecladoState[RIGHT] = 1;
      break;

    case GLUT_KEY_UP:
      tecladoState[UP] = 1;
      break;

    case GLUT_KEY_DOWN:
      tecladoState[DOWN] = 1;
      break;

    default:
        break;
    }
  }

void teclado(unsigned char key, int x, int y){
   switch (key) {
     case 27:  // Tecla ESC
      exit(0);
      break;

     case 'm':
     case 'M':
        tecladoState[M] = !tecladoState[M];

     case ' ': // Para pular
      tecladoState[SPACE] = 1;
      break;

    case 'P':
    case 'p':
      pausar = !pausar;

    default:
         break;
   }
}

void posicionaCamera(int x, int y) {
    xMouse = x;
    yMouse = y;
    glutPostRedisplay();
}

void click(int button, int state, int x, int y){
  //printf("x: %d ||  y: %d tela como jogar: %d\n",x, y, telaNUM[TELACOMOJOGAR]);
  if (telaNUM[TELAINICIAL]){
    if (x <= 756 && x >= 563){
      if (y <= 406 && y >= 337){ // JOGO
        telaNUM[TELAINICIAL] = 0;
        telaNUM[TELAJOGO] = 1;
        pausar = 0;
      }
      else if (y <= 500 && y >= 432 ){ // COMO JOGAR
        telaNUM[TELAINICIAL] = 0;
        telaNUM[TELACOMOJOGAR] = 1;
      }
      else if (y <= 605 && y >= 538 ){ // sair
        telaNUM[TELAINICIAL] = 0;
        telaNUM[TELAJOGO] = 0;
        telaNUM[TELASAIR] = 1;
      }
    }
  }

  else if (telaNUM[TELACOMOJOGAR]){
    if (x <= 1233 && x >= 1115){
      if (y <= 146 && y >= 32){
        telaNUM[TELAINICIAL] = 1;
        telaNUM[TELACOMOJOGAR] = 0;
        telaNUM[TELAJOGO] = 0;
        telaNUM[TELASAIR] = 0;
      }
    }
  }

  else if (telaNUM[TELASAIR]){
    if (x <= 1224 && x >= 946){
      if (y <= 661 && y >= 445){
        exit(0);
      }
    }
    else if (x <= 179 && x >= 66){
      if (y <= 701 && y >= 596){
        telaNUM[TELAINICIAL] = 0;
        telaNUM[TELAJOGO] = 1;
        telaNUM[TELASAIR] = 0;
        inicializa();
      }
    }
  }

  glutPostRedisplay();
}

// Função para atualizar movimentos etc
void atualiza (int idx){
  FILE *arq;
  int result;

  if (!pausar){
    pontuacao += 0.1;
    correPersonagem(tecladoState[SPACE]);
    alteraPosicaoZObstaculos();
    transladaPistas();
    eliminaObstaculo(0);
    verificaColisao();

    if (vidas <= 0){
      telaNUM[TELAJOGO] = 0;
      telaNUM[TELASAIR] = 1;
      pausar = 1;

      arq = fopen("pontuacao.txt", "a");  // Cria um arquivo texto para gravação

      if (arq == NULL) // Se nào conseguiu criar
      {
         printf("Problemas na CRIACAO do arquivo\n");
         return;
      }

        // A funcao 'fprintf' devolve o número de bytes gravados
      // ou EOF se houve erro na gravação
      result = fprintf(arq,"Pontuacao %f\n", pontuacao);
      if (result == EOF)
      printf("Erro na Gravacao\n");
      fclose(arq);
    }
    //printf("X: %f\n y: %f \n\n\n", posiX, posiY);
    //printf("Pé: %f\n\n", peDoBoneco);
  }

  glutPostRedisplay();
  glutTimerFunc(17, atualiza, 0); // 60frames por segundo
}

void redimensiona(int w, int h){
   larguraJanela = w;
   alturaJanela = h;

   glViewport (0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode (GL_PROJECTION | GL_DEPTH_BUFFER_BIT);
   glLoadIdentity();

   if (telaNUM[TELAINICIAL]){
     ConfiguraProjecao('o');
   }
   else{
     //fovy          aspect          Znear  Zfar
      ConfiguraProjecao('p');
   }
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();

   //glTranslatef (0.0, 0.0, -5.0);
}

int main(int argc, char** argv){
   glutInit(&argc, argv);
   glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
   glutInitWindowSize (1366, 768);
   glutInitWindowPosition (100, 100);
   glutCreateWindow ("TheRunner");

   inicializa();

   glutReshapeFunc(redimensiona);
   glutKeyboardFunc(teclado);

   glutSpecialFunc(tecladoSpecialDOWN);
   glutSpecialUpFunc(tecladoSpecialUP);
   glutMouseFunc(click);

   glutPassiveMotionFunc(posicionaCamera);

   glutDisplayFunc(desenha);

   glutTimerFunc(0, atualiza, 0);

   glutMainLoop();
   return 0;
}
