#include <SOIL/SOIL.h>
#include <string.h>

#define YMIN 2  // coordenada absoluta
#define YMAX -0.25  // coordenada absoluta
#define MAX_MAP 8

// Guarda os tamanhos da janela
int larguraJanela, alturaJanela;

// Guarda as partes do corpo que serão desenhadas
int vidas;

// Contador de pontos
float pontuacao;

// Variável que define a dificuldade do jogo
GLuint dificuldade;

// Nomeia os indices das texturas
typedef enum textura {
  CORRETIVO = 0,
  BORRACHA,
  INICIAL,
  BOTAO,
  CHAO,
  SKYBOX,
  COMOJOGAR,
  SAIR
} TEXTURA;


typedef enum telaNum {
  TELAINICIAL = 0,
  TELASAIR,
  TELACOMOJOGAR,
  TELAJOGO
} TELANUM;

// Vetor para "gavetas" de texturas
extern GLuint texturas[20];

extern GLfloat peDoBoneco;

extern GLfloat posiX, posiY, posiZ;

extern char pausar;

// Procedimentos
// Carrega um arquivo de textura passado como parâmetro
// void carregaTextura(GLuint *textura, char *nomeDoArquivo);

// Inicializa as texturas
void initTexturas(void);

// Procediemnto para fazer excrita na tela
void escreveTexto(void *fonte, char *s, float x, float y, float z);

// Configura a tela como ortogonal ou perspecitiva
void ConfiguraProjecao(char tipo);

// Desenha textura a partir da sua posição
void desenhaTextura(int x, int y, int posi);

// Função adaptada do código do Nate Robins
GLMmodel* preparaModelo(char *nomeDoArquivo);
