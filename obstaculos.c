#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <SOIL/SOIL.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include "obstaculos.h"
#include "glm.h"
#include "utilidades.h"
#include "objetosEmCodigo/corretivo.h"
#include "objetosEmCodigo/borracha.h"

// Procedimentos
void iniciaListaObstaculos() {
  // Alocando memória para a lista de obstáculos
  listaObstaculos = (LISTA *)malloc(sizeof(LISTA));

  // Cria a cabeça da lista
  listaObstaculos->cabeca = (NO *)malloc(sizeof(NO));

  // Cria um nó após o cabeça
  listaObstaculos->cabeca->proximo = (NO *)malloc(sizeof(NO));

  // O nó anterior ao cabeça não existe, então é NULL
  listaObstaculos->cabeca->anterior = NULL;

  // O nó anterior ao próximo nó do nó-cabeça é o próprio nó cabeça
  listaObstaculos->cabeca->proximo->anterior = listaObstaculos->cabeca;

  // Nesse momento, o nó seguinte ao cabeça da lista também é o último nó
  listaObstaculos->ultimo = listaObstaculos->cabeca->proximo;

  // Agora, o próximo ao último nó não existe
  listaObstaculos->ultimo->proximo = NULL;

  // Inicia o tamanho da lista como 0
  listaObstaculos->tamanho = 0;

  // TESTE: inicia com número máximo de elementos 1
  dificuldade = 10;
}

GLfloat criaAnguloAleatorio(){
  // Determina o ângulo para o qual a borracha será virada
  // 0 para esquerda
  // 1 para direita
  GLint lado = rand() % 2;

  if(lado)
    return 90;
  else
    return -90;

}

GLfloat deprecated_criaPosicaoXAleatoria(){
  srand(time(0));
  return -7 + rand() % 16;
}

void determinaPosicaoEAngulo(OBSTACULO *dadosObstaculo) {
  // O ângulo é determinado randomicamente de formas diferentes
  // de acordo com cada tipo de objeto
  if(dadosObstaculo->obstaculoList == corretivoList)
    dadosObstaculo->anguloDeRotacao = rand() % 361;
  else if(dadosObstaculo->obstaculoList == borrachaList)
    dadosObstaculo->anguloDeRotacao = criaAnguloAleatorio();

  // A posição Z é determinada além do frustum e do FOG
  dadosObstaculo->posicaoZ = -40.0;
  // A posição Y deve ser a mesma do chão
  dadosObstaculo->posicaoY = -12.0;
  // A posição X deve ser aleatória
  dadosObstaculo->posicaoX = posiX;
}

void determinaObstaculo(OBSTACULO *dadosObstaculo) {
  // Gera um número aleatório para decidir qual
  // tipo de obstáculo será gerado
  srand(time(0));
  GLuint qualObstaculo = rand() % 2;

  // Se qualObstaculo tem valor 0, gera-se um corretivo
  if(!qualObstaculo)
    dadosObstaculo->obstaculoList = corretivoList;
  // senão, gera-se uma borracha
  else
    dadosObstaculo->obstaculoList = borrachaList;

}

void determinaSemiComprimentos(OBSTACULO *dadosObstaculo) {
  // Semicomprimentos medidos empiricamente :)
  // Caso o obstáculo seja um corretivo
  if(dadosObstaculo->obstaculoList == corretivoList){
    dadosObstaculo->semiComprimentoX = 1.0;
    dadosObstaculo->semiComprimentoY = 0.4;
    dadosObstaculo->semiComprimentoZ = 0.6;
  }
  // Caso seja uma borracha
  else if (dadosObstaculo->obstaculoList == borrachaList){
    dadosObstaculo->semiComprimentoX = 0.5;
    dadosObstaculo->semiComprimentoY = 0.6;
    dadosObstaculo->semiComprimentoZ = 0.4;
  }

}

void adicionaObstaculoNaLista() {
  // Se o tamanho da lista for maior que 0
  if(listaObstaculos->tamanho){
    // Cria um novo nó após o último
    listaObstaculos->ultimo->proximo = (NO *)malloc(sizeof(NO));

    // Diz que o nó anterior do próximo nó do último da lista é o próprio último
    listaObstaculos->ultimo->proximo->anterior = listaObstaculos->ultimo;

    // O último nó da lista se torna o próximo nó ao último
    listaObstaculos->ultimo = listaObstaculos->ultimo->proximo;

    // Agora, o próximo ao último nó não existe
    listaObstaculos->ultimo->proximo = NULL;
  }

  // Determina de que tipo será o próximo obstáculo
  determinaObstaculo(&listaObstaculos->ultimo->dadosObstaculo);

  // Determina os semicomprimentos do objeto criado
  determinaSemiComprimentos(&listaObstaculos->ultimo->dadosObstaculo);

  // Gera os dados do obstáculo
  determinaPosicaoEAngulo(&listaObstaculos->ultimo->dadosObstaculo);

  // Incrementa o tamanho da lista
  listaObstaculos->tamanho++;

}

void alteraPosicaoZObstaculos() {
  // Nó móvel para percorrer a lista
  NO *obstaculoAtual;

  // Percorre a lista incrementando a posicaoZ de cada obstáculo
  for(obstaculoAtual = listaObstaculos->cabeca->proximo; obstaculoAtual != NULL; obstaculoAtual = obstaculoAtual->proximo)
    obstaculoAtual->dadosObstaculo.posicaoZ += 0.3;

}

void desenhaObstaculos() {
  if(listaObstaculos == NULL)
    return ;

  if(listaObstaculos->tamanho == 0)
    return ;

  // Nó móvel para percorrer a lista
  NO *obstaculoAtual;

  // Percorre a lista desenhando os obstáculos
  for(obstaculoAtual = listaObstaculos->cabeca->proximo; obstaculoAtual != NULL; obstaculoAtual = obstaculoAtual->proximo) {
    // Se o obstáculo atual for um corretivo, o desenha usando a displayList de corretivo
    if(obstaculoAtual->dadosObstaculo.obstaculoList == corretivoList)
      desenhaCorretivo(obstaculoAtual->dadosObstaculo.posicaoX, obstaculoAtual->dadosObstaculo.posicaoY, obstaculoAtual->dadosObstaculo.posicaoZ, obstaculoAtual->dadosObstaculo.anguloDeRotacao);

    // e o mesmo caso seja uma borracha
    else if(obstaculoAtual->dadosObstaculo.obstaculoList == borrachaList)
      desenhaBorracha(obstaculoAtual->dadosObstaculo.posicaoX, obstaculoAtual->dadosObstaculo.posicaoY, obstaculoAtual->dadosObstaculo.posicaoZ, obstaculoAtual->dadosObstaculo.anguloDeRotacao);
  }
}

void spawnaObstaculo(int idx) {
  if(pausar){
    glutTimerFunc(1500, spawnaObstaculo, 0);
    return ;
  }

  if(listaObstaculos->tamanho < dificuldade)
    adicionaObstaculoNaLista();

  //printf("X: %f\n y: %f\n\n\n", posiX, posiY);

  glutTimerFunc(1500, spawnaObstaculo, 0);

}

void eliminaObstaculo(int apagaLista) {
  if(listaObstaculos == NULL)
    return ;

  if(listaObstaculos->tamanho == 0)
    return ;

  // Nó móvel para percorrer a lista
  NO *obstaculoAtual = NULL;

  // Salva o obstáculo a ser retirado da lista
  NO *salvaObstaculo = NULL;

  if(!apagaLista){
    // Percorre a lista removendo os obstáculos que já passaram da câmera
    for(obstaculoAtual = listaObstaculos->cabeca->proximo; obstaculoAtual != NULL; obstaculoAtual = obstaculoAtual->proximo){
      if(obstaculoAtual->dadosObstaculo.posicaoZ >= 5.0){
        // Salvando o obstáculo
        salvaObstaculo = obstaculoAtual;

        // Desligando o nó da lista
        obstaculoAtual->anterior->proximo = obstaculoAtual->proximo;

        if(listaObstaculos->ultimo == salvaObstaculo)
          listaObstaculos->ultimo = salvaObstaculo->anterior;
        else
          obstaculoAtual->proximo->anterior = obstaculoAtual->anterior;

        listaObstaculos->tamanho--;

        // Liberando a memória do obstáculo
        if(salvaObstaculo != NULL)
          free(salvaObstaculo);

        break;
      }
    }

    return ;
  }
  // Caso haja colisão de algum obstáculo com o boneco,
  // apaga a lista inteira começando do último
  else if(apagaLista){
    for(obstaculoAtual = listaObstaculos->ultimo; obstaculoAtual != listaObstaculos->cabeca; obstaculoAtual = listaObstaculos->ultimo){
      // Salva o obstáculo para posterior liberação
      salvaObstaculo = obstaculoAtual;

      // Aponta o próximo nó do nó anterior ao obstáculo corrente como NULL
      obstaculoAtual->anterior->proximo = NULL;

      // O último nó da lista se torna o anterior ao nó corrente
      listaObstaculos->ultimo = obstaculoAtual->anterior;

      // Por segurança, aponta o obstáculo atual para NULL
      obstaculoAtual = NULL;

      // Decrementa o tamanho da lista em 1
      listaObstaculos->tamanho--;

      // Apaga o nó da lista
      free(salvaObstaculo);
    }
  }

}

void verificaColisao() {
  if(listaObstaculos == NULL)
    return ;

  if(listaObstaculos->tamanho == 0)
    return ;

  // Nó móvel para percorrer a lista
  NO *obstaculoAtual = NULL;

  // Variáveis para guardar a área que o obstáculo ocupa
  // para posterior comparação de posição entre o obstáculo
  // e o boneco
  GLfloat posicaoX1, posicaoX2;
  GLfloat posicaoY1;
  GLfloat posicaoZ1, posicaoZ2;

  for(obstaculoAtual = listaObstaculos->cabeca->proximo; obstaculoAtual != NULL; obstaculoAtual = obstaculoAtual->proximo){
    posicaoX1 = obstaculoAtual->dadosObstaculo.posicaoX + obstaculoAtual->dadosObstaculo.semiComprimentoX;
    posicaoX2 = obstaculoAtual->dadosObstaculo.posicaoX - obstaculoAtual->dadosObstaculo.semiComprimentoX;

    posicaoY1 = obstaculoAtual->dadosObstaculo.posicaoY - obstaculoAtual->dadosObstaculo.semiComprimentoY;

    posicaoZ1 = obstaculoAtual->dadosObstaculo.posicaoZ + obstaculoAtual->dadosObstaculo.semiComprimentoZ;
    posicaoZ2 = obstaculoAtual->dadosObstaculo.posicaoZ - obstaculoAtual->dadosObstaculo.semiComprimentoZ;

    if(posiX >= posicaoX2 && posiX <= posicaoX1){
      if(posiZ >= posicaoZ2 && posiZ <= posicaoZ1){
        if(peDoBoneco >= posicaoY1 /*|| peDoBoneco <= posicaoY2*/){
          if(obstaculoAtual->dadosObstaculo.obstaculoList == corretivoList)
              //  printf("Corretivo\n");
              vidas -= 2;
          else
             vidas --;
            //printf("Borracha\n");

          eliminaObstaculo(1);
        }
      }
    }
  }
}
