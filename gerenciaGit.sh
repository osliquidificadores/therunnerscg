#!/bin/bash

# Pede que o usuário escolha o que deseja fazer
echo -e "O que você deseja?\n1) git pull (baixar arquivos do repositório)\n2) git push (atualizar repositório)\n"
read RESPOSTA

# Executa comandos de acordo com a escolha anterior do usuário
if [[ $RESPOSTA -eq 1 ]]; then
  git pull
elif [[ $RESPOSTA -eq 2 ]]; then
  git add .
  echo -e "O que você deseja comentar sobre a nova atualização no repositório?\n\n"
  read COMENTARIO
  git commit -m "$COMENTARIO"
  git push -u origin master
fi

echo "Bye!"
sleep 2
clear
exit
