# TheRunners

Jogo desenvolvido como trabalho para a disciplina de Computação Gráfica (1/2017), ministrada pelo Profº Flávio Coutinho, do curso de Engenharia da Computação do Centro Federal de Educação Tecnológica de Minas Gerais (CEFET-MG).

O desenvolvimento desse jogo visava o aprendizado da biblioteca gráfica OpenGL, programação estruturada, noções de espaço 3D, modelagem 3D, texturização, iluminação, boas práticas de programação, e agregação de nota para o semestre na disciplina.

### Pré-requisitos para compilação do código-fonte

Para compilação do código fonte é necessária a instalação prévia das bibliotecas freeglut e glew, bem como um compilador de código-fonte em C, como o GCC.

### Executando

Para compilação e execução do código, basta conceder permissão de execução ao arquivo
```
Makefile
```

via terminal, com o comando

```
make run
```

Para excluir os arquivos .o basta executar o seguinte comando via terminal

```
make clean
```

Para excluir o executável do jogo basta a execução do seguinte comando via terminal
```
make rmproper
```

## Itens extras
* Sistema de vidas
* Sistema de pontuação
* Telas acessórias
    * Inicial;
    * Instruções;
    * Créditos
* Highscore em arquivo
* Modelos em formato .obj (feitos no Blender 3D)
* Salto
* Head bobbing diferenciado (quando o boneco pula)
* Skybox (de lápis)


## Feito com

* [freeglut](http://freeglut.sourceforge.net/) - Parte da biblioteca do OpenGL
* [glew](http://glew.sourceforge.net/) - Parte da biblioteca do OpenGL
* [Atom](https://atom.io/) - Editor de texto
* [Blender 3D](https://www.blender.org/) - Modelagem 3D, animação, renderização e pós-produção
* [Gimp](https://www.gimp.org/) - Programa de manipulação de imagem

## Autores

* **Arthur Novaes** -  [ArthurN](https://gitlab.com/ArthurN)
* **Marcelo Ferreira Cândido** - [marsCanHaveLife](https://gitlab.com/marsCanHaveLife)
