#!/bin/bash

# Compilando códigos-fonte
gcc -o main.o -c main.c -Wall
gcc -o glm.o -c glm.c -Wall
gcc -o utilidades.o -c utilidades.c -Wall
gcc -o personagem.o -c personagem.c -Wall
gcc -o corretivo.o -c objetosEmCodigo/corretivo.c -Wall
gcc -o borracha.o -c objetosEmCodigo/borracha.c -Wall
gcc -o obstaculos.o -c obstaculos.c -Wall
gcc -o pista.o -c objetosEmCodigo/pista.c -Wall
gcc -o skybox.o -c objetosEmCodigo/skybox.c -Wall

# Fazendo a linkedição
gcc -o jogo.exe glm.o utilidades.o personagem.o pista.o skybox.o corretivo.o borracha.o obstaculos.o main.o -lglut -lGL -lGLU -lSOIL -lm

# Removendo arquivos *.o
rm *.o

# Executando o jogo
./jogo.exe

# Removendo o jogo
#rm jogo.exe
