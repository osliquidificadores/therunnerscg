#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <string.h>
#include <math.h>

// Função responsávell por fazer o desenho hierárquico do boneco
void desenhaPersonagem(double posiX, double posiY, double posiZ, int vidasRecebe);

// Função responsável por fazer a movimentação hierárquica do boneco, perns e braços
void correPersonagem(int tecladoStateUP);
