#include "personagem.h"

int anguloTronco = 270, anguloPerDir = 30, anguloPerEsq = 330, anguloPerDirZ = 0, anguloPerEsqZ = 0;
char cresDecresEsq = +5;
char cresDecresDir = +5;
char cresDecresBrac = +5;
int anguloBraco = 30;
GLfloat angulo; // para desenhar a cabeça

void desenhaPersonagem(double posiX, double posiY, double posiZ, int vidasRecebe){
  // T R O N C O
  glPushMatrix();
    glScalef(0.5, 0.5, 0.5);
    glLineWidth(3);
    glPushMatrix();
      glTranslatef (0.0, -2, 0.0);
      glRotatef ((GLfloat) anguloTronco, 0.0, 0.0, 1.0);
      glTranslatef (posiY, posiX, posiZ);
      glPushMatrix();
        glBegin(GL_LINES);
          glVertex3f(0,0,0);
          glVertex3f(1,0,0);
        glEnd();
      glPopMatrix();

      //@TODO: Fazer movimentação em Z
      // Perna direita
      if (vidasRecebe > 0){
    glPushMatrix();
      glTranslatef (1.0, 0.0, 0.0);
      glRotatef ((GLfloat) anguloPerDir, 0.0, 0.0, 1.0);
      glTranslatef (0.0, 0.0, 0.0);// onde vai desenhar

        //glScalef (1.0, 0.4, 1.0);
        //cglutWireCube (1.0);
        glBegin(GL_LINES);
          glVertex3f(0,0,0);
          glVertex3f(1,0,0);
        glEnd();
      glPopMatrix();

      // Perna esquerda
      if (vidasRecebe > 1){
      glPushMatrix();
      glTranslatef (1.0, 0.0, 0.0);
      glRotatef((GLfloat) anguloPerEsq, 0.0, 0.0, 1.0);
      glTranslatef (0.0, 0.0, 0.0);

        glBegin(GL_LINES);
          glVertex3f(0,0,0);
          glVertex3f(1,0,0);
        glEnd();
      glPopMatrix();

      // BRAÇOS
      // Braço direito
      if (vidasRecebe > 2){
      glPushMatrix();
      glTranslatef (0.0, 0.0, 0.0);
      glRotatef((GLfloat) anguloBraco, 0.0, 0.0, 1.0);
      glTranslatef (0.0, 0.0, 0.0);
        glBegin(GL_LINES);
          glVertex3f(0,0,0);
          glVertex3f(1,0,0);
        glEnd();
      glPopMatrix();

      // Braço esquerdo (espelhado no direito)
      if (vidasRecebe == 4){
      glPushMatrix();
      glTranslatef (0.0, 0.0, 0.0);
      glRotatef((GLfloat) -anguloBraco, 0.0, 0.0, 1.0);
      glTranslatef (0.0, 0.0, 0.0);
        glBegin(GL_LINES);
          glVertex3f(0,0,0);
          glVertex3f(1,0,0);
        glEnd();
      glPopMatrix();
    }
    }
    }
    }

      // C A B E Ç A
      glPushMatrix();
      glTranslatef (-0.35, 0.0, 0.0);
      glRotatef(0.0, 0.0, 0.0, 1.0);
      glTranslatef (0.0, 0.0, 0.0);
      glBegin(GL_TRIANGLE_FAN);
        glutWireSphere(0.3, 200, 200);
      glPopMatrix();
    glPopMatrix();
  glPopMatrix();
}

void correPersonagem(int tecladoStateUP){
  // movimentação
  // PERNA DIREITA
  if (anguloPerDir <= 20 && tecladoStateUP == 0)
    cresDecresDir = +5;
  else if (anguloPerDir >= 60 && tecladoStateUP == 0)
    cresDecresDir = -5;
  else if (tecladoStateUP)
    anguloPerDir = 60;

    anguloPerDir = (anguloPerDir + cresDecresDir) % 360;

  // PERNA ESQUERDA
    if (anguloPerEsq <= 300 && tecladoStateUP == 0)
      cresDecresEsq = +5;
    else if (anguloPerEsq >= 340 && tecladoStateUP == 0)
      cresDecresEsq = -5;
    else if (tecladoStateUP)
      anguloPerEsq = 300;

    anguloPerEsq = (anguloPerEsq + cresDecresEsq) % 360;

    // BRAÇOS
    if (anguloBraco <= 20)
      cresDecresBrac = +3;
    else if (anguloBraco >= 40)
      cresDecresBrac = -3;

    anguloBraco = (anguloBraco + cresDecresBrac) % 360;

    glutSwapBuffers();
}
