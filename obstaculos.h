
typedef struct obstaculo {
  // Recebe um número de uma gaveta para
  // um objeto (corretivo ou borracha) aleatoriamente
  GLuint obstaculoList;
  // Define a posição do obstáculo no espaço
  GLfloat posicaoX, posicaoY, posicaoZ;
  // Define o semicomprimento (metade da dimensão) de cada dimensão do objeto
  GLfloat semiComprimentoX, semiComprimentoY, semiComprimentoZ;
  // Define o angulo aleatório de rotação do obstáculo
  // O obstáculo só poderá ser rotacionado no eixo Y, para manter um padrão
  GLfloat anguloDeRotacao;
} OBSTACULO;

// Estruturas definindo uma lista e seus nós
typedef struct noListaObstaculos {
  struct noListaObstaculos *anterior;
  OBSTACULO dadosObstaculo;
  struct noListaObstaculos *proximo;
} NO;

typedef struct listaDeObstaculos {
  NO *cabeca;
  GLuint tamanho;
  NO *ultimo;
} LISTA;

// Cria uma estrutura de lista para os obstáculos
LISTA *listaObstaculos;

// Procedimentos

// Inicia a lista de obstáculos randômicos
void iniciaListaObstaculos();
// Cria um angulo aleatório para a borracha
GLfloat criaAnguloAleatorio();
// Cria uma posição X aleatória para os objetos
GLfloat deprecated_criaPosicaoXAleatoria();
// Determina a posição que o obstáculo inicia e seu ângulo
void determinaPosicaoEAngulo(OBSTACULO *dadosObstaculo);
// Determina se o obstáculo será um corretivo ou borracha
void determinaObstaculo(OBSTACULO *dadosObstaculo);
// Adiciona um novo nó-obstáculo à lista
void adicionaObstaculoNaLista();
// Altera a tranalação dos obstáculos no eixo Z
void alteraPosicaoZObstaculos();
// Desenha os obstáculos seguindo a lista deles
void desenhaObstaculos();
// Determina se um obstáculo deve aparecer, sendo adicionado à lista
void spawnaObstaculo(int idx);
// Elimina o obstáculo após ele passar da câmera
void eliminaObstaculo(int apagaLista);
// Verifica colisão entre o boneco e os objetos
void verificaColisao();
