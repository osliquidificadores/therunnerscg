#include <stdlib.h>
#include <time.h>
#include <SOIL/SOIL.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include "obstaculos.h"
#include "glm.h"
#include "utilidades.h"
#include "objetosEmCodigo/corretivo.h"
#include "objetosEmCodigo/borracha.h"


// Procedimentos
void iniciaListaObstaculos(){

  listaObstaculos = (LISTA *)malloc(sizeof(LISTA));
  // Cria o primeiro nó da lista
  (*listaObstaculos).primeiro = (NO *)malloc(sizeof(NO));

  // Nesse momento, o primeiro nó também é o último
  (*listaObstaculos).ultimo = (*listaObstaculos).primeiro;

  // O primeiro nó da lista não possui anterior e, no momento,
  // também não possui próximo
  (*(*listaObstaculos).primeiro).anterior = NULL;
  (*(*listaObstaculos).primeiro).proximo = NULL;

  // Inicia tamanho da lista como 0
  (*listaObstaculos).tamanho = 0;

  // TESTE: inicia com número máximo de elementos 1
  dificuldade = 10;
}

GLfloat criaAnguloAleatorio(){
  // Determina o ângulo para o qual a borracha será virada
  // 0 para esquerda
  // 1 para direita
  GLint lado = rand() % 2;

  if(lado)
    return 90;
  else
    return -90;

}

GLfloat criaPosicaoXAleatoria(){
  srand(time(0));
  return -7 + rand() % 16;
}

void determinaPosicaoEAngulo(OBSTACULO * obstaculo) {
  // O ângulo é determinado randomicamente de formas diferentes
  // de acordo com cada tipo de objeto
  if(obstaculo->obstaculoList == corretivoList)
    (*(*(*listaObstaculos).ultimo).conteudo).anguloDeRotacao = rand() % 361;
  else if((*(*(*listaObstaculos).ultimo).conteudo).obstaculoList == borrachaList)
    (*(*(*listaObstaculos).ultimo).conteudo).anguloDeRotacao = criaAnguloAleatorio();

  // A posição Z é determinada além do frustum e do FOG
  (*(*(*listaObstaculos).ultimo).conteudo).posicaoZ = -15.0;
  // A posição Y deve ser a mesma do chão
  (*(*(*listaObstaculos).ultimo).conteudo).posicaoY = YMIN;
  // A posição X deve ser aleatória
  (*(*(*listaObstaculos).ultimo).conteudo).posicaoX = criaPosicaoXAleatoria();
}

void determinaObstaculo() {
  // Gera um número aleatório para decidir qual
  // tipo de obstáculo será gerado
  srand(time(0));
  GLuint qualObstaculo = rand() % 2;

  // Se qualObstaculo tem valor 0, gera-se um corretivo
  if(!qualObstaculo)
    (*(*(*listaObstaculos).ultimo).conteudo).obstaculoList = corretivoList;
  // senão, gera-se uma borracha
  else
    (*(*(*listaObstaculos).ultimo).conteudo).obstaculoList = borrachaList;
}

void adicionaObstaculoNaLista(){
  // Pega o último obstáculo da lista e preenche seu conteúdo
  listaObstaculos->ultimo->proximo = (NO*)malloc(sizeof(NO));
  listaObstaculos->ultimo = listaObstaculos->ultimo->proximo;
  listaObstaculos->ultimo->conteudo = (OBSTACULO *)malloc(sizeof(OBSTACULO));
  listaObstaculos->ultimo->proximo = NULL;
  // (*(*listaObstaculos).ultimo).conteudo = (OBSTACULO *)malloc(sizeof(OBSTACULO));

  // Incrementa o tamanho da lista
  (*listaObstaculos).tamanho++;

  // Determina de que tipo será o próximo obstáculo
  determinaObstaculo();

  // Gera os dados do obstáculo
  determinaPosicaoEAngulo(listaObstaculos->ultimo->conteudo);

  // Cria um próximo nó



  // (*(*listaObstaculos).ultimo).proximo = (NO *)malloc(sizeof(NO));
  // // O nó anterior do nó criado é o nó atual (último)
  // (*(*(*listaObstaculos).ultimo).proximo).anterior = (*listaObstaculos).ultimo;
  // // O próximo nó do nó criado não existe ainda
  // (*(*(*listaObstaculos).ultimo).proximo).proximo = NULL;
  // // O nó criado passa a ser o último da lista
  // (*listaObstaculos).ultimo = (*(*listaObstaculos).ultimo).proximo;

}

#include <stdio.h>

void alteraPosicaoZObstaculos(){
  // Percorre a lista de obstáculos incrementando cada posicaoZ
  // de cada obstáculo
  NO* atual;
  printf("vai iterar nos obstaculos\n");
  for (atual = listaObstaculos->primeiro;
       atual != NULL;
       atual = atual->proximo) {
         printf("iterou \t");
         atual->conteudo->posicaoZ += 0.2;
       }
  printf("\n");
  // NO * atual;
  // for(atual = (*listaObstaculos).primeiro; atual != NULL; atual = (*atual).proximo)
    // (*(*atual).conteudo).posicaoZ += 0.2;
}

void desenhaObstaculos(){
  // Percorre a lista desenhando os obstáculos
  for(obstaculoAtual = (*listaObstaculos).primeiro; obstaculoAtual != NULL; obstaculoAtual = (*obstaculoAtual).proximo){
    // Se o obstáculo atual for um corretivo, o desenha usando a displayList de corretivo
    if((*(*obstaculoAtual).conteudo).obstaculoList == corretivoList)
      desenhaCorretivo((*(*obstaculoAtual).conteudo).posicaoX, (*(*obstaculoAtual).conteudo).posicaoY, (*(*obstaculoAtual).conteudo).posicaoZ, (*(*obstaculoAtual).conteudo).anguloDeRotacao);

    // e o mesmo caso seja uma borracha
    else if((*(*obstaculoAtual).conteudo).obstaculoList == borrachaList)
      desenhaBorracha((*(*obstaculoAtual).conteudo).posicaoX, (*(*obstaculoAtual).conteudo).posicaoY, (*(*obstaculoAtual).conteudo).posicaoZ, (*(*obstaculoAtual).conteudo).anguloDeRotacao);
  }
}

void spawnaObstaculo(){
  if((*listaObstaculos).tamanho < dificuldade)
    adicionaObstaculoNaLista();

  glutTimerFunc(4000, spawnaObstaculo, 0);
}
