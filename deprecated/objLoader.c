#include <GL/gl.h>
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include "objLoader.h"

// Procedimentos
void criaArquivosOBJ()
{
  // Separando arquivos relativos ao objeto corretivo
  system("chmod +x separaOBJ.sh");
  system("./separaOBJ.sh");
}

void abreArquivoOBJ(char nome[])
{
  FILE *arquivoOBJ;

  // if ((arquivoOBJ = fopen(nome, "r")) == NULL) {
  //   printf("Erro ao abrir o arquivo %s\n", nome);
  //   exit(1);
  // }

}

void coletaVerticesMesh(FILE *arquivoOBJ)
{
  vertices = 0;

  verticesCorretivo = (VMESH *)malloc(sizeof(VMESH));
  while(!feof(arquivoOBJ)){
    if(vertices)
      verticesCorretivo = (VMESH *)realloc(verticesCorretivo, (vertices + 1) * sizeof(VMESH));

    fscanf(arquivoOBJ, "v %f %f %f\n", &verticesCorretivo[vertices].x,
      &verticesCorretivo[vertices].y, &verticesCorretivo[vertices].z);

      vertices++;
  }

  system("rm verticesCorretivo.obj");
}

void coletaFacesMesh(FILE *arquivoOBJ)
{
  faces = 0;

  facesCorretivo = (FACE *)malloc(sizeof(FACE));
  while(!feof(arquivoOBJ)){
    if(faces)
      facesCorretivo = (FACE *)realloc(facesCorretivo, (faces + 1) * sizeof(FACE));

    fscanf(arquivoOBJ, "f %d/%d/%d %d/%d/%d %d/%d/%d\n",
      &facesCorretivo[faces].mesh[0],
      &facesCorretivo[faces].textura[0],
      &facesCorretivo[faces].normal[0],
      &facesCorretivo[faces].mesh[1],
      &facesCorretivo[faces].textura[1],
      &facesCorretivo[faces].normal[1],
      &facesCorretivo[faces].mesh[2],
      &facesCorretivo[faces].textura[2],
      &facesCorretivo[faces].normal[2]);

    faces++;
  }

  system("rm facesCorretivo.obj");
}
