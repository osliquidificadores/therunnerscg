#!/bin/bash

# Corretivo
cat arquivosBlender/arquivosOBJ/corretivo/corretivo.obj | grep "v " > arquivosBlender/arquivosOBJ/corretivo/vertices.obj
cat arquivosBlender/arquivosOBJ/corretivo/corretivo.obj | grep "vt " > arquivosBlender/arquivosOBJ/corretivo/textura.obj
cat arquivosBlender/arquivosOBJ/corretivo/corretivo.obj | grep "vn " > arquivosBlender/arquivosOBJ/corretivo/normais.obj
cat arquivosBlender/arquivosOBJ/corretivo/corretivo.obj | grep "f " > arquivosBlender/arquivosOBJ/corretivo/faces.obj

# Borracha
cat arquivosBlender/arquivosOBJ/borracha/borracha.obj | grep "v " > arquivosBlender/arquivosOBJ/borracha/vertices.obj
cat arquivosBlender/arquivosOBJ/borracha/borracha.obj | grep "vt " > arquivosBlender/arquivosOBJ/borracha/textura.obj
cat arquivosBlender/arquivosOBJ/borracha/borracha.obj | grep "vn " > arquivosBlender/arquivosOBJ/borracha/normais.obj
cat arquivosBlender/arquivosOBJ/borracha/borracha.obj | grep "f " > arquivosBlender/arquivosOBJ/borracha/faces.obj
