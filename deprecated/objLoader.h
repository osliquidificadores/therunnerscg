
// Estruturas que definem o objeto
// v x y z
typedef struct verticeMesh {
  GLfloat x, y, z;
} VMESH;

// vt u v
typedef struct verticeTextura {
  GLfloat u, v;
} VTEXTURA;

// vn x y z
typedef struct vetorNormal {
  GLfloat x, y, z;
} VNORMAL;

typedef struct material {
  GLfloat diffuse;
  GLfloat specular;
} MATERIAL;

// f v1/vt1/vn1 v2/vt2/vn2 v3/vt3/vn3
typedef struct face {
  GLint mesh[3], textura[3], normal[3];
} FACE;

typedef struct objeto {
  VMESH *verticesObjeto;
  VTEXTURA *verticesTextura;
  VNORMAL *vetoresNormais;
  FACE *faces;
  // TODO: Criar vetor de materiais para o objeto
} OBJETO;

// Matrizes
VMESH *verticesCorretivo;
VTEXTURA *texturaCorretivo;
VNORMAL *normaisCorretivo;
FACE *facesCorretivo;

GLint vertices;
GLint faces;

// Procedimentos
void criaArquivosOBJ();
void abreArquivoOBJ(char nomeArquivo[]);
void coletaVerticesMesh(FILE *arquivoOBJ);
void coletaFacesMesh(FILE *arquivoOBJ);
