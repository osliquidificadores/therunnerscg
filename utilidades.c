#include <GL/glew.h>
#include <GL/freeglut.h>
#include <stdio.h>

#include "glm.h"
#include "utilidades.h"

GLuint texturas[20];
GLfloat posiX = (GLfloat) 0.0;
GLfloat posiY = (GLfloat) YMIN;
GLfloat posiZ = (GLfloat) 0.0;

char pausar = 1;

// Procedimentos
int carregaTextura(char* nomeDoArquivo) {
  int textura = SOIL_load_OGL_texture(nomeDoArquivo, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);

  if (textura == 0)
    printf("Erro ao carregar textura '%s': %s\n", nomeDoArquivo, SOIL_last_result());

  return textura;
}

void initTexturas(void) {
  texturas[CORRETIVO] = carregaTextura("arquivosBlender/arquivosTexturas/corretivo.png");
  texturas[BORRACHA] = carregaTextura("arquivosBlender/arquivosTexturas/borrachaTextura.png");
  texturas[INICIAL] = carregaTextura("imagens/menuinicial.jpg");
  texturas[BOTAO] = carregaTextura("imagens/botao.png");
  texturas[CHAO] = carregaTextura("imagens/chao.png");
  texturas[SKYBOX] = carregaTextura("arquivosBlender/arquivosTexturas/lapis.png");
  texturas[COMOJOGAR] = carregaTextura("imagens/ComoJogar.png");
  texturas[SAIR] = carregaTextura("imagens/sair.png");
}

void desenhaTextura(int x, int y, int posi){
  glColor3f (1, 1, 1);
  glEnable(GL_TEXTURE_2D);
  // Começa a usar a textura que criamos		    // Começa a usar a textura que criamos
  glBindTexture(GL_TEXTURE_2D, texturas[posi]);
   glBegin(GL_TRIANGLE_FAN);
      // Associamos um canto da textura para cada vértice		        // Associamos um canto da textura para cada vértice
      glTexCoord2f(0, 0); glVertex3f(-x, -y,  0);
      glTexCoord2f(1, 0); glVertex3f( x, -y,  0);
      glTexCoord2f(1, 1); glVertex3f( x,  y,  0);
      glTexCoord2f(0, 1); glVertex3f(-x,  y,  0);
  glEnd();
  glDisable(GL_TEXTURE_2D);
}

void escreveTexto(void *fonte, char *s, float x, float y, float z){
  glRasterPos3f(x, y, z);
  for(int i = 0; i < strlen(s); i++){
    glutBitmapCharacter(fonte, s[i]);
  }
}

void ConfiguraProjecao(char tipo){

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  switch (tipo) {
    case 'o': // ortogonal
      glDisable(GL_DEPTH_TEST);
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glOrtho(-100, 100, -100, 100, -1, 1); // Define a tela
      break;

    case 'p': // Perpectiva
      glDisable(GL_BLEND);
      glEnable(GL_DEPTH_TEST);
      gluPerspective(65.0, (GLfloat) larguraJanela/(GLfloat) alturaJanela, 1.0, 20.0);
      break;

    default:
      break;
  }

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

}

// Função adaptada do código do Nate Robins
GLMmodel* preparaModelo(char *nomeDoArquivo){
  GLMmodel *pmodel = NULL;

  if (!pmodel) {
	  pmodel = glmReadOBJ(nomeDoArquivo);

    if (!pmodel)
      exit(0);

	  //glmUnitize(pmodel);
	  glmFacetNormals(pmodel);
	  glmVertexNormals(pmodel, 90.0);
  }

  return pmodel;
}
